// apitest is a small demo of reading a protobuf saved as YAML.
//
// For instance, [API configurations in Google Cloud Endpoints] are actually
// protobufs, but stored neither as binary or ascii proto, nor as JSON, but
// as YAML.
//
// [API configurations in Google Cloud Endpoints]: https://cloud.google.com/endpoints/docs/grpc/grpc-service-config
package main // badc0de.net/pkg/apitest

import (
	"github.com/ghodss/yaml"
	"google.golang.org/genproto/googleapis/api/serviceconfig" // source: github.com/googleapis/googleapis -> google/api/auth.proto etc
	"github.com/golang/protobuf/jsonpb"

	"bytes"
	"fmt"
	"io/ioutil"
)

func main() {
	f, err := ioutil.ReadFile("api_config.yaml")
	if err != nil {
		panic(err)
	}

	jsn, err := yaml.YAMLToJSON(f)
	fmt.Printf("%+v %+v\n", err, string(jsn))
	
	service := serviceconfig.Service{}
	//err = jsonpb.UnmarshalString(string(jsn), &service)
	unmarshaler := jsonpb.Unmarshaler{
		AllowUnknownFields: true,
	}
	buf := bytes.NewBuffer(jsn)
	err = unmarshaler.Unmarshal(buf, &service)
	fmt.Printf("%+v %+v\n", err, service)
}
