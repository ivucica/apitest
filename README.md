apitest is a small demo of reading a protobuf saved as YAML.

For instance, [API configurations in Google Cloud Endpoints] are actually
protobufs, but stored neither as binary or ascii proto, nor as JSON, but
as YAML.

[API configurations in Google Cloud Endpoints]: https://cloud.google.com/endpoints/docs/grpc/grpc-service-config
